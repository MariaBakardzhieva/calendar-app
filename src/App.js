import './App.scss';
import { CalendarApp } from './components/CalendarApp.jsx';

export const App = () => {
  return (
    <div className="App">
     <CalendarApp />
    </div>
  );
}

