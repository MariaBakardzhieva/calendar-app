import { createSlice } from "@reduxjs/toolkit";
import events from "../events.json";

export const eventsSlice = createSlice({
  name: "events",
  initialState: {
    ...events,
  },
  reducers: {
    addEvent: (state, { payload }) => ({
      ...state,
      [payload.key]: payload.value,
    }),
    deleteEvent: (state, { payload }) => {
      const newState = {};
      Object.keys(state).forEach((key) => {
        if (payload.key !== key) {
          newState[key] = state[key];
        }
      });
      return {
        ...newState,
      };
    },
    updateEvent: (state, { payload }) => {
      return {
        ...state,
        [payload.key]: payload.value,
      };
    },
  },
});

export const { addEvent, deleteEvent, updateEvent } = eventsSlice.actions;

export default eventsSlice.reducer;
