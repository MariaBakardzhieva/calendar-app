import React, { Fragment, useState } from "react";
import { useCalendar } from "../hooks/useCalendar";
import { Button } from "./Button";
import "./Calendar.scss";
import { useSelector, useDispatch } from "react-redux";
import { addEvent, deleteEvent, updateEvent } from "../store/eventsSlice";
import DisplayEvent from "./DisplayEvent";
import EventForm from "./EventForm";

export const Calendar = () => {
  const dispatch = useDispatch();
  const events = useSelector((state) => state.events);

  const {
    calendarRows,
    selectedDate,
    today,
    daysName,
    monthName,
    incrementMonth,
  } = useCalendar();

  const [selectedEvent, setSelectedEvent] = useState({});
  const [createdEvent, setCreatedEvent] = useState({});
  const [state, setState] = useState("");

  const dateClickHandler = (date) => {
    if (events.hasOwnProperty(date)) {
      setSelectedEvent(events[date]);
      setCreatedEvent({});
      setState("view");
    } else {
      setState("create");
      setCreatedEvent({ title: "", description: "", date });
    }
  };

  const handleChange = (key, value) => {
    setCreatedEvent({
      ...createdEvent,
      [key]: value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const updateFunction = state === "edit" ? updateEvent : addEvent;
    dispatch(
      updateFunction({
        key: createdEvent.date,
        value: createdEvent,
      })
    );
    setCreatedEvent({});
    setSelectedEvent(createdEvent);
    setState("view");
  };

  const handleDelete = (key) => {
    dispatch(
      deleteEvent({
        key,
      })
    );
    setSelectedEvent({});
    setState("");
  };

  const toggleEdit = (event) => {
    setState("edit");
    setCreatedEvent(event);
  };

  const handleChangeMonth = (factor) => {
    incrementMonth(factor);
    setSelectedEvent({});
  };

  return (
    <Fragment>
      <div className="calendar">
        <Button
          src="images/left-arrow.png"
          alt="left-arrow"
          handleClick={() => handleChangeMonth(-1)}
        />
        <p className="calendar__title">
          {" "}
          {`${
            monthName[selectedDate.getMonth()]
          } - ${selectedDate.getFullYear()}`}
        </p>
        <Button
          src="images/right-arrow.png"
          alt="right-arrow"
          handleClick={() => handleChangeMonth(1)}
        />
      </div>
      <div className="table">
        <table className="table__body">
          <thead className="table__body__months">
            <tr>
              {daysName.map((day) => (
                <th key={day}>{day}</th>
              ))}
            </tr>
          </thead>
          <tbody className="table__body__days">
            {Object.values(calendarRows).map((cols) => {
              return (
                <tr key={cols[0].date}>
                  {cols.map(({ date, value, classes }) => (
                    <td
                      key={date}
                      className={`${classes} ${today === date && "today"} ${
                        events.hasOwnProperty(date) && "hasEvent"
                      }`}
                      onClick={() => dateClickHandler(date)}
                    >
                      {value}
                    </td>
                  ))}
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
      <div className="calendar__event">
        {state === "view" && (
          <DisplayEvent
            handleEdit={toggleEdit}
            selectedEvent={selectedEvent}
            handleDelete={handleDelete}
          />
        )}
        {(state === "edit" || state === "create") && (
          <EventForm
            createdEvent={createdEvent}
            handleSubmit={handleSubmit}
            handleChange={handleChange}
          />
        )}
      </div>
    </Fragment>
  );
};
