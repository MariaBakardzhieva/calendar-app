import React from "react";
import "./Button.scss";

export const Button = ({src, alt, handleClick}) => {
  return(
    <button className="button" onClick={handleClick}>
      <img src={src} alt={alt}/>
    </button>
  )
}