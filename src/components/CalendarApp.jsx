import React from 'react';
import { Calendar } from './Calendar.jsx';
import "./CalendarApp.scss";

export const CalendarApp = () => {
  return (
    <div className="calendar-app">
      <div className="calendar-app__container">
        <Calendar />
      </div>
    </div>
  )
}