import React from "react";

export default function CreateEventForm({
  createdEvent,
  handleSubmit,
  handleChange,
}) {
  return (
    <form className="calendar__event__form" onSubmit={handleSubmit}>
      <h3>Create event for: {createdEvent.date}</h3>
      <input
        type="text"
        value={createdEvent.title}
        onChange={(e) => handleChange("title", e.target.value)}
      />
      <input
        type="text"
        value={createdEvent.description}
        onChange={(e) => handleChange("description", e.target.value)}
      />
      <button>Submit</button>
    </form>
  );
}
