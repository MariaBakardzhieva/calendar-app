import React from "react";

export default function DisplayEvent({
  selectedEvent,
  handleEdit,
  handleDelete,
}) {
  return (
    <div className="calendar__event__info">
      <h1>{selectedEvent.title}</h1>
      <p>{selectedEvent.description}</p>
      <button onClick={() => handleEdit(selectedEvent)}>Edit Event</button>
      <button onClick={() => handleDelete(selectedEvent.date)}>
        Delete Event
      </button>
    </div>
  );
}
